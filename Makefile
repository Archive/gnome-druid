CFLAGS=`gnome-config --cflags gnomeui` -g -Wall -O2
LIBS=`gnome-config --libs gnomeui` 

OBJS=\
	gnome-druid.o \
	gnome-druid-page.o \
	gnome-druid-page-start.o \
	gnome-druid-page-standard.o \
	gnome-druid-page-finish.o

all: test-druid

lib: libdruid.a

clean: 
	rm -f *o test-druid *a

test-druid: $(OBJS) test.o
	gcc -o test-druid $(OBJS) $(LIBS) test.o

libdruid.a: $(OBJS)
	rm -f libdruid.a
	ar cr libdruid.a $(OBJS)


