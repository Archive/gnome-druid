#include "gnome-druid.h"
#include "gnome-druid-page.h"
#include "gnome-druid-page-start.h"
#include "gnome-druid-page-standard.h"
#include "gnome-druid-page-finish.h"
#define MY_TEXT "This will help you set up a new printer for your machine. \nPlease click \"Next\" to begin."
GtkWidget *page1;
static void
destroy_event (GtkWidget *widget, gpointer data)
{
	gtk_main_quit ();
}
static void
cancel_event (GnomeDruid *druid, gpointer data)
{
	gtk_main_quit ();
}
gint
main (int argc, char *argv[])
{
	GtkWidget *window;
	GtkWidget *druid;
	GtkWidget *page;
	GdkImlibImage *image1, *image2;

	gnome_init ("TEST-DRUID", "0.0.1", argc, argv);
	image1 = gdk_imlib_load_image ("printer.png");
	image2 = gdk_imlib_load_image ("/usr/share/pixmaps/redhat/shadowman-48.png");
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_signal_connect (GTK_OBJECT (window), "destroy", destroy_event, NULL);
	druid = gnome_druid_new ();
	gtk_signal_connect (GTK_OBJECT (druid), "cancel", cancel_event, NULL);
	gtk_container_add (GTK_CONTAINER (window), druid);
	page1 = gnome_druid_page_start_new_with_vals ("Printer Configuration Tool", MY_TEXT, image2, image1);
	gnome_druid_append_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (page1));
	gnome_druid_set_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (page1));
	page = gnome_druid_page_standard_new_with_vals ("foo", NULL);
	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox), gtk_button_new_with_label ("BORP!!!!!"), TRUE, TRUE, 0);
	gnome_druid_append_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (page));
	page = gnome_druid_page_standard_new_with_vals ("foo", image2);
	gtk_box_pack_start (GTK_BOX (GNOME_DRUID_PAGE_STANDARD (page)->vbox), gtk_button_new_with_label ("BORP2!!!!!"), TRUE, TRUE, 0);
	gnome_druid_append_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (page));
	page = gnome_druid_page_finish_new_with_vals ("Printer Configuration Tool", "This is a test", image2, image1);
	gnome_druid_append_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (page));
	gtk_widget_show_all (window);

	gtk_main ();
	return 0;
}
